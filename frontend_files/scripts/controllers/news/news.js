'use strict';

angular.module('marketing.news.controllers')
	.controller('newsController', function ($scope, news) {
		$scope.news = news;
	});
