'use strict';

angular.module('marketing.controllers')
	.controller('homeController', function ($scope, $state, countries, slides, ENV) {
		$scope.countries = countries;
		$scope.selectedCountry = 'shqiperi';
		$scope.slides = slides;
		$scope.search = function(){
			$state.go('marketing.country', {filter: $scope.text, country: $scope.selectedCountry});
		};
	});
