'use strict';

angular.module('marketing.about.controllers')
	.controller('aboutController', function ($scope, about) {
		$scope.about = about;
	});