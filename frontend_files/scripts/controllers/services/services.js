'use strict';

angular.module('marketing.services.controllers')
	.controller('servicesController', function ($scope, categories, services, currentCategory) {
		$scope.categories = categories;
		$scope.currentCategory = currentCategory;
		$scope.services = services;
		$scope.changeCategory = function(id) {
			if($scope.currentCategory == id) {
				$scope.currentCategory = undefined;
			}
			else {
				$scope.currentCategory = id;
			}
		}
	});
