'use strict';

angular.module('marketing.services.controllers')
	.controller('serviceController', function ($scope, service) {
		$scope.service = service;
	});
