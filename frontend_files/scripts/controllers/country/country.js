'use strict';

angular.module('marketing.country.controllers')
	.controller('countryController', function ($scope, filter, categories, country, $location, $anchorScroll) {
		$scope.country = country?country:'shqiperi';
		$scope.categories = categories;
		$scope.text = filter;
		$scope.currentCategory = undefined;
		$scope.changeCategory = function(id) {
			if($scope.currentCategory == id) {
				$scope.currentCategory = undefined;
			}
			else {
				$scope.currentCategory = id;
				$location.hash('businesses');
				$anchorScroll();
				scrollBy(0,-50);
			}
		};
	});
