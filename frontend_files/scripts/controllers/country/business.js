'use strict';

angular.module('marketing.country.controllers')
	.controller('businessController', function ($scope, business) {
		$scope.business = business;
	});
