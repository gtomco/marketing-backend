'use strict';

angular.module('marketing.contact.controllers')
	.controller('contactController', function ($scope, resourceService, $state, $http, vcRecaptchaService) {
    $scope.sendEmail = function(){
      //$scope.email.captcha = vcRecaptchaService.getResponse();
      resourceService.forResource('email')
        .save($scope.email)
          .then(function(result){
            //console.log(result);
            $state.go('marketing.home');
          });
    };
	});
