'use strict';

angular.module('marketing.events.controllers')
	.controller('eventController', function ($scope, event) {
		$scope.event = event;
	});
