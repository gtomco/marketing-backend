'use strict';

angular.module('marketing.events.controllers')
	.controller('eventsController', function ($scope, countries, events) {
		$scope.events = events;
		$scope.countries = countries;
		$scope.currentCountry = undefined;
		$scope.changeCountry = function(id) {
			if($scope.currentCountry == id) {
				$scope.currentCountry = undefined;
			}
			else {
				$scope.currentCountry = id;
			}
		}
	});
