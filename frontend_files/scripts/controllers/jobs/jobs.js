'use strict';

angular.module('marketing.jobs.controllers')
	.controller('jobsController', function ($scope, categories, types, jobs, currentCategory, currentType) {
		$scope.categories = categories;
		$scope.types = types;
		$scope.currentCategory = currentCategory;
		$scope.currentType = currentType;
		$scope.jobs = jobs;
		$scope.changeCategory = function(id) {
			if($scope.currentCategory == id) {
				$scope.currentCategory = undefined;
			}
			else {
				$scope.currentCategory = id;
			}
		};

		$scope.changeType = function(id) {
			if($scope.currentType == id) {
				$scope.currentType = undefined;
			}
			else {
				$scope.currentType = id;
			}
		};
	});
