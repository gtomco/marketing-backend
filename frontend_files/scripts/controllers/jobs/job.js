'use strict';

angular.module('marketing.jobs.controllers')
	.controller('jobController', function ($scope, job, ENV) {
		$scope.job = job;
		$scope.path = ENV.imagesEndpoint + job.path + '.' + job.ext;
	});
