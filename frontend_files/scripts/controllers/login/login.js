'use strict';
angular.module('marketing.login.controllers')
  .controller('loginController', function ($scope, authorizationService, $timeout, $state) {
    $scope.itemSource = {
      username: '',
      password: ''
    };

    $scope.login = function () {
      authorizationService.login($scope.itemSource)
        .then(function () {
          $timeout(function () {
            $state.go('marketing.home');
          });
        })
        .catch(function () {
          $scope.errorMessage = 'Username or password not valid.';
        });
    };
  });
