﻿'use strict';
angular.module('marketing.controllers')
	.controller('menuController', function ($scope, resourceService, MARKETING) {
		resourceService.forResource('serviceCategories')
			.get()
				.then(function(result){
					$scope.services = result.categories;
				});
		resourceService.forResource('jobCategories')
			.get()
				.then(function(result){
					$scope.jobTypes = result.types;
				});
	});
