'use strict';
angular.module('marketing.controllers', [
  'marketing.modals.controllers',
  'marketing.login.controllers',
  'marketing.admin.controllers',
  'marketing.country.controllers',
  'marketing.contact.controllers',
  'marketing.events.controllers',
  'marketing.news.controllers',
  'marketing.services.controllers',
  'marketing.consulting.controllers',
  'marketing.about.controllers',
  'marketing.jobs.controllers'
]);
