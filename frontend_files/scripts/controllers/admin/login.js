'use strict';

angular.module('marketing.admin.controllers')
	.controller('loginController', function ($scope, $http, ENV, $state) {

		$scope.login = function(){
			$http.post(ENV.baseEndpoint + "admin/login", {email: $scope.email, password: $scope.password})
				.then(function(result){
					$state.go('marketing.admin.dashboard');
				});
		};
	});
