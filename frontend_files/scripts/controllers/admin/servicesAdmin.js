'use strict';

angular.module('marketing.admin.controllers')
	.controller('servicesAdminController', function ($scope, services, categories, resourceService, $state) {
		$scope.services = services;
		$scope.categories = categories;

		$scope.delete = function(id, key){
			resourceService.forResource('services')
				.delete(id)
					.then(function(result){
						$scope.services.splice(key, 1);
					});
		};
	})
	.controller('serviceAdminController', function ($scope, $http, service, categories, resourceService, $state, ENV) {
		$scope.isNew = service?false:true;
		if(!$scope.isNew){
			$scope.service = service;
			$scope.categories = categories;
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'service/' + service.id
			};
		}
		else {
			$scope.service = {};
		}
		$scope.categories = categories;

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('services')
					.save($scope.service)
						.then(function(result){
							$state.go('marketing.admin.serviceEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('services')
					.update($scope.service.id,$scope.service)
						.then(function(result){
							$state.go('marketing.admin.services');
						});
			}
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.service.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.service.images.push(JSON.parse($message));
		};
	});
