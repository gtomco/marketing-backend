'use strict';

angular.module('marketing.admin.controllers')
	.controller('consultingsAdminController', function ($scope, consultings, resourceService, $state) {
		$scope.consultings = consultings;

		$scope.delete = function(id, key){
			resourceService.forResource('consulting')
				.delete(id)
					.then(function(result){
						$scope.consultings.splice(key, 1);
					});
		};
	})
	.controller('consultingAdminController', function ($scope, $http, consulting, resourceService, $state, ENV) {
		$scope.isNew = consulting?false:true;
		if(!$scope.isNew){
			$scope.consulting = consulting;
			$scope.marker = {};
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'consulting/' + consulting.id
			};
		}
		else {
			$scope.consulting = {};
		}

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('consulting')
					.save($scope.consulting)
						.then(function(result){
							$state.go('marketing.admin.consultingEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('consulting')
					.update($scope.consulting.id,$scope.consulting)
						.then(function(result){
							$state.go('marketing.admin.consultings');
						});
			}
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.consulting.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.consulting.images.push(JSON.parse($message));
		};
	});
