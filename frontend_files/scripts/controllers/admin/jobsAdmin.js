'use strict';

angular.module('marketing.admin.controllers')
	.controller('jobsAdminController', function ($scope, jobs, categories, countries, resourceService, $state) {
		$scope.jobs = jobs;
		$scope.categories = categories;
		$scope.countries = countries;

		$scope.delete = function(id, key){
			resourceService.forResource('jobs')
				.delete(id)
					.then(function(result){
						$scope.jobs.splice(key, 1);
					});
		};
	})
	.controller('jobAdminController', function ($scope, $http, job, categories, countries, resourceService, $state, ENV) {
		$scope.isNew = job?false:true;
		if(!$scope.isNew){
			$scope.job = job;
			$scope.categories = categories;
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'job/' + job.id
			};
			$scope.flowPathPdf = {
				target: ENV.pdfUploadEndpoint + job.id
			};
			$scope.path = ENV.imagesEndpoint + job.path + '.' + job.ext;
		}
		else {
			$scope.job = {};
		}
		$scope.categories = categories;
		$scope.countries = countries;

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('jobs')
					.save($scope.job)
						.then(function(result){
							$state.go('marketing.admin.jobEdit',{id: result.id});
						});
			}
			else {
				resourceService.forResource('jobs')
					.update($scope.job.id,$scope.job)
						.then(function(result){
							$state.go('marketing.admin.jobs');
						});
			}
		};

		$scope.deleteImage = function(id){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.job.image = null;
					});
		};

		$scope.deletepdf = function(id){
			resourceService.forResource('pdf')
				.delete(id)
					.then(function(result){
						$scope.job.path = null;
						$scope.path = null;
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.job.image = JSON.parse($message);
		};

		$scope.addPdf = function($file, $message, $flow){
			$scope.job.path = JSON.parse($message).path;
			$scope.job.ext = JSON.parse($message).ext;
			$scope.path = ENV.imagesEndpoint + $scope.job.path + '.' + $scope.job.ext;
		};
	});
