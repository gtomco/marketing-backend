'use strict';

angular.module('marketing.admin.controllers')
	.controller('newssAdminController', function ($scope, newss, resourceService, $state) {
		$scope.newss = newss;

		$scope.delete = function(id, key){
			resourceService.forResource('news')
				.delete(id)
					.then(function(result){
						$scope.newss.splice(key, 1);
					});
		};
	})
	.controller('newsAdminController', function ($scope, $http, news, resourceService, $state, ENV) {
		$scope.isNew = news?false:true;
		if(!$scope.isNew){
			$scope.news = news;
			$scope.marker = {};
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'news/' + news.id
			};
		}
		else {
			$scope.news = {};
		}

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('news')
					.save($scope.news)
						.then(function(result){
							$state.go('marketing.admin.newsEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('news')
					.update($scope.news.id,$scope.news)
						.then(function(result){
							$state.go('marketing.admin.news');
						});
			}
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.news.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.news.images.push(JSON.parse($message));
		};
	});
