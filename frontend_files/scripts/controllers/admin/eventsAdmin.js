'use strict';

angular.module('marketing.admin.controllers')
	.controller('eventsAdminController', function ($scope, events, countries, resourceService, $state) {
		$scope.events = events;
		$scope.countries = countries;

		$scope.delete = function(id, key){
			resourceService.forResource('events')
				.delete(id)
					.then(function(result){
						$scope.events.splice(key, 1);
					});
		};
	})
	.controller('eventAdminController', function ($scope, event, countries, resourceService, $state, $http, ENV) {
		$scope.isNew = event?false:true;
		if(!$scope.isNew){
			$scope.event = event;
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'event/' + event.id
			};
		}
		else {
			$scope.event = {};
		}
		$scope.countries = countries;

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('events')
					.save($scope.event)
						.then(function(result){
							$state.go('marketing.admin.eventEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('events')
					.update($scope.event.id,$scope.event)
						.then(function(result){
							$state.go('marketing.admin.events');
						});
			}
		};

		$scope.updateMarker = function($event){
			$scope.event.coords_x = $event.latLng.lat();
			$scope.event.coords_y = $event.latLng.lng();
			$http.get("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + $event.latLng.lat() + "," + $event.latLng.lng() + "&sensor=false")
				.then(function(r){
					if(r.data.results[0]){
						$scope.event.address = r.data.results[0].formatted_address;
					}
			});
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.event.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.event.images.push(JSON.parse($message));
		};

	});
