'use strict';
angular.module('marketing.admin.controllers')
	.controller('optionsAdminController', function ($scope, slides, businesses, resourceService) {
		$scope.slides = slides;
		$scope.businesses = businesses;

		$scope.newSlide = {
			priority: parseInt($scope.slides[($scope.slides.length)-1].priority)+1
		};

		$scope.deleteSlide = function(id, key){
			resourceService.forResource('slides')
				.delete(id)
					.then(function(result){
						$scope.slides.splice(key, 1);
						$scope.newSlide = {
							priority: parseInt($scope.slides[$scope.slides.length-1].priority)+1
						};
					});
		};

		$scope.saveSlide = function(id, key){
			if(!id){
				$scope.newSlide.business_id = $scope.newSlide.business.id;
				resourceService.forResource('slides')
					.save($scope.newSlide)
						.then(function(result){
							$scope.slides.push(result);
							$scope.newSlide = {
								priority: parseInt($scope.slides[$scope.slides.length-1].priority)+1
							};
						});
			}
			else {
				$scope.slides[key].business_id = $scope.slides[key].business.id;
				resourceService.forResource('slides')
					.update($scope.slides[key].id,$scope.slides[key])
						.then(function(result){
							$scope.slides[key] = result;
						});
			}
		};
	});
