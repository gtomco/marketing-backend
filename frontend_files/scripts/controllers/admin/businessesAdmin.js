'use strict';

angular.module('marketing.admin.controllers')
	.controller('businessesAdminController', function ($scope, businesses, categories, countries, resourceService, $state) {
		$scope.businesses = businesses;
		$scope.categories = categories;
		$scope.countries = countries;

		$scope.delete = function(id, key){
			resourceService.forResource('businesses')
				.delete(id)
					.then(function(result){
						$scope.businesses.splice(key, 1);
					});
		};
	})
	.controller('businessAdminController', function ($scope, $http, business, categories, countries, resourceService, $state, ENV) {
		$scope.isNew = business?false:true;
		if(!$scope.isNew){
			$scope.business = business;
			$scope.marker = {};
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'business/' + business.id
			};
		}
		else {
			$scope.business = {};
		}
		$scope.categories = categories;
		$scope.countries = countries;

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('businesses')
					.save($scope.business)
						.then(function(result){
							$state.go('marketing.admin.businessEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('businesses')
					.update($scope.business.id,$scope.business)
						.then(function(result){
							$state.go('marketing.admin.businesses');
						});
			}
		};

		$scope.updateMarker = function($event){
			$scope.business.coords_x = $event.latLng.lat();
			$scope.business.coords_y = $event.latLng.lng();
			$http.get("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + $event.latLng.lat() + "," + $event.latLng.lng() + "&sensor=false")
				.then(function(r){
					if(r.data.results[0]){
						$scope.business.address = r.data.results[0].formatted_address;
					}
			});
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.business.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.business.images.push(JSON.parse($message));
		};
	});
