'use strict';

angular.module('marketing.admin.controllers')
	.controller('adminController', function ($scope, ENV, $http, $state) {
		$scope.logout = function(){
			$http.get(ENV.baseEndpoint + 'admin/logout')
				.then(function(result){
					$state.go('marketing.login');
				});
		};
	});
