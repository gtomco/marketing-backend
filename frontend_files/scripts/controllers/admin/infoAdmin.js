'use strict';

angular.module('marketing.admin.controllers')
	.controller('infosAdminController', function ($scope, abouts, resourceService, $state) {
		$scope.abouts = abouts;

		$scope.delete = function(id, key){
			resourceService.forResource('about')
				.delete(id)
					.then(function(result){
						$scope.abouts.splice(key, 1);
					});
		};
	})
	.controller('infoAdminController', function ($scope, $http, about, resourceService, $state, ENV) {
		$scope.isNew = about?false:true;
		if(!$scope.isNew){
			$scope.about = about;
			$scope.marker = {};
			$scope.flowPath = {
				target: ENV.imageUploadEndpoint + 'about/' + about.id
			};
		}
		else {
			$scope.about = {};
		}

		$scope.save = function(id, key){
			if($scope.isNew){
				resourceService.forResource('about')
					.save($scope.about)
						.then(function(result){
							$state.go('marketing.admin.infoEdit',{id: result.id});
						});
			}
			else{
				resourceService.forResource('about')
					.update($scope.about.id,$scope.about)
						.then(function(result){
							$state.go('marketing.admin.infoEdit');
						});
			}
		};

		$scope.deleteImage = function(id, key){
			resourceService.forResource('images')
				.delete(id)
					.then(function(result){
						$scope.about.images.splice(key, 1);
					});
		};

		$scope.pushImage = function($file, $message, $flow){
			$scope.about.images.push(JSON.parse($message));
		};
	});
