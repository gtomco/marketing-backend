'use strict';

angular.module('marketing.consulting.controllers')
	.controller('consultingController', function ($scope, consultings) {
		$scope.consultings = consultings;
	});
