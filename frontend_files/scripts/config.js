angular.module('marketing.config', [])
	.constant('ENV', {
		apiEndpoint:'http://www.magnetbiznes.dev/api/',
		baseEndpoint: 'http://www.magnetbiznes.dev/',
		imageUploadEndpoint: 'http://www.magnetbiznes.dev/api/images/',
		pdfUploadEndpoint: 'http://www.magnetbiznes.dev/api/pdf/',
		imagesEndpoint: 'http://www.magnetbiznes.dev/public_files/uploads/'
});
