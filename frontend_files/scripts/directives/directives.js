'use strict';
angular.module('marketing.directives', ['marketing.filters', 'marketing.constants', 'marketing.config', 'pdf'])
	.directive('myPdfViewerToolbar', ['pdfDelegate', function(pdfDelegate) {
		return {
			restrict: 'E',
			template:
				'<div class="clearfix form-inline">' +
					'<div class="col-sm-4">' +
						'<div class="btn-group btn-group-justified" role="group">' +
							'<a href="" ng-click="prev()" class="btn btn-md btn-default"><span class="glyphicon glyphicon-chevron-left"></span> <span>Back</span></a>' +
							'<a href="" ng-click="next()" class="btn btn-md btn-default"><span>Next</span> <span class="glyphicon glyphicon-chevron-right"></span></a>' +
						'</div>' +
					'</div>' +
					'<div class="col-sm-4">' +
						'<div class="input-group">' + 
							'<span class="input-group-addon">Page</span>' +
							'<input type="text" class="form-control" min=1 ng-model="currentPage" ng-change="goToPage()" style="text-align:center;">' +
							'<span class="input-group-addon">/ {{getPageCount()}}</span>' +
						'</div>' +
					'</div>' +
					'<div class="col-sm-4">' +
						'<a class="pull-right btn btn-md btn-success" href="{{$parent.path}}"><span class="fa fa-download"></span> <span class="">Shkarko PDF</span></a>' +
					'</div>' +
				'</div>',
			scope: { pageCount: '=' },
			link: function(scope, element, attrs) {
				var id = attrs.delegateHandle;
				scope.currentPage = 1;

				scope.prev = function() {
					pdfDelegate
						.$getByHandle(id)
							.prev();
					updateCurrentPage();
				};
				scope.next = function() {
					pdfDelegate
						.$getByHandle(id)
							.next();
					updateCurrentPage();
				};
				scope.goToPage = function() {
					pdfDelegate
						.$getByHandle(id)
							.goToPage(scope.currentPage);
				};
				scope.getPageCount = function() {
					return pdfDelegate
						.$getByHandle(id)
							.getPageCount();
				};

				var updateCurrentPage = function() {
					scope.currentPage = pdfDelegate
						.$getByHandle(id)
							.getCurrentPage();
				};
			}
		};
	}]);
