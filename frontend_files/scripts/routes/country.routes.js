'use strict';
angular.module('marketing.country.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.country', {
				url: '/shteti/{country}',
				params: {
					country: 'shqiperi',
					filter: null
				},
				templateUrl: '../../frontend_files/views/country/index.html',
				controller: 'countryController',
				pageTitle: 'Kerkim sipas shtetit - magnetbiznes.com',
				resolve: {
					categories: function(resourceService, $stateParams){
						return resourceService.forResource('categories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					country: function(resourceService, $stateParams){
						return resourceService.forResource('countries')
							.get($stateParams.country,'businesses.category,businesses.images,cities')
								.then(function(result){
									return result.country;
								});
					},
					filter: function($stateParams){
						return $stateParams.filter;
					}
				}
			})
			.state('marketing.business', {
				url: '/business/{id}',
				templateUrl: '../../frontend_files/views/country/business.html',
				controller: 'businessController',
				pageTitle: 'Bizneset - magnetbiznes.com',
				resolve: {
					business: function(resourceService, $stateParams){
						return resourceService.forResource('businesses')
							.get($stateParams.id,'country,images')
								.then(function(result){
									return result.business;
								});
					}
				}
			});
	});
