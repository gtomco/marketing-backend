'use strict';
angular.module('marketing.routes', [
	'marketing.admin.routes',
	'marketing.country.routes',
	'marketing.contact.routes',
	'marketing.events.routes',
	'marketing.news.routes',
	'marketing.services.routes',
	'marketing.consulting.routes',
	'marketing.about.routes',
	'marketing.jobs.routes'
]);

angular.module('marketing.routes')
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('marketing', {
				abstract: true,
				template: '<div ui-view></div>',
				pageTitle: '-'
			})
			.state('marketing.home', {
				url: '/',
				templateUrl: '../../frontend_files/views/home/home.html',
				controller: 'homeController',
				pageTitle: 'Faqja Kryesore - magnetbiznes.com',
				resolve: {
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.get()
								.then(function(result){
									return result.countries;
								});
					},
					slides: function(resourceService){
						return resourceService.forResource('slides')
							.get()
								.then(function(result){
									return result.slides;
								});
					}
				}
			});
			$urlRouterProvider.otherwise('/');
	});
