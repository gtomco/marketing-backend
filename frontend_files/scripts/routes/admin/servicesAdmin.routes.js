'use strict';
angular.module('marketing.servicesAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.services', {
				url: '/services',
				templateUrl: '../../frontend_files/views/admin/services.html',
				controller: 'servicesAdminController',
				pageTitle: 'Shërbimet',
				resolve: {
					services: function(resourceService){
						return resourceService.forResource('services')
							.query({include: 'category'})
								.then(function(result){
									return result.services;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('serviceCategories')
							.query()
								.then(function(result){
									return result.categories;
								});
					}
				}
			})
			.state('marketing.admin.serviceEdit', {
				url: '/service/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/service.html',
				controller: 'serviceAdminController',
				pageTitle: 'Shërbim',
				resolve: {
					service: function(resourceService, $stateParams){
						return resourceService.forResource('services')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.service;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('serviceCategories')
							.query()
								.then(function(result){
									return result.categories;
								});
					}
				}
			})
			.state('marketing.admin.serviceNew', {
				url: '/service/new',
				templateUrl: '../../frontend_files/views/admin/service.html',
				controller: 'serviceAdminController',
				pageTitle: 'Shërbim',
				resolve: {
					service: function(resourceService, $stateParams){
						return null;
					},
					categories: function(resourceService){
						return resourceService.forResource('serviceCategories')
							.query()
								.then(function(result){
									return result.categories;
								});
					}
				}
			});
	});
