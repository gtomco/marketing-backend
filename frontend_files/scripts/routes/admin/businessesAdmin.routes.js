'use strict';
angular.module('marketing.businessesAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.businesses', {
				url: '/businesses',
				templateUrl: '../../frontend_files/views/admin/businesses.html',
				controller: 'businessesAdminController',
				pageTitle: 'Businesses',
				resolve: {
					businesses: function(resourceService){
						return resourceService.forResource('businesses')
							.query({include: 'country,category,city'})
								.then(function(result){
									return result.businesses;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('categories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query({include: 'cities'})
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.businessEdit', {
				url: '/business/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/business.html',
				controller: 'businessAdminController',
				pageTitle: 'Business',
				resolve: {
					business: function(resourceService, $stateParams){
						return resourceService.forResource('businesses')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.business;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('categories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query({include: 'cities'})
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.businessNew', {
				url: '/business/new',
				templateUrl: '../../frontend_files/views/admin/business.html',
				controller: 'businessAdminController',
				pageTitle: 'Business',
				resolve: {
					business: function(resourceService, $stateParams){
						return null;
					},
					categories: function(resourceService){
						return resourceService.forResource('categories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			});
	});
