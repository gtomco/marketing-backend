'use strict';
angular.module('marketing.infoAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.info', {
				url: '/info',
				templateUrl: '../../frontend_files/views/admin/abouts.html',
				controller: 'infosAdminController',
				pageTitle: 'Informacione',
				resolve: {
					abouts: function(resourceService){
						return resourceService.forResource('about')
							.query()
								.then(function(result){
									return result.about;
								});
					}
				}
			})
			.state('marketing.admin.infoEdit', {
				url: 'info/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/about.html',
				controller: 'infoAdminController',
				pageTitle: 'Informacion',
				resolve: {
					about: function(resourceService, $stateParams){
						return resourceService.forResource('about')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.about;
								});
					}
				}
			})
			.state('marketing.admin.infoNew', {
				url: '/info/new',
				templateUrl: '../../frontend_files/views/admin/about.html',
				controller: 'infoAdminController',
				pageTitle: 'Informacion',
				resolve: {
					about: function(resourceService){
						return null;
					}
				}
			});
	});
