'use strict';
angular.module('marketing.jobsAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.jobs', {
				url: '/jobs',
				templateUrl: '../../frontend_files/views/admin/jobs.html',
				controller: 'jobsAdminController',
				pageTitle: 'Punësimet',
				resolve: {
					jobs: function(resourceService){
						return resourceService.forResource('jobs')
							.query({include: 'type,country'})
								.then(function(result){
									return result.jobs;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('jobCategories')
							.query()
								.then(function(result){
									return result.types;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.jobEdit', {
				url: '/job/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/job.html',
				controller: 'jobAdminController',
				pageTitle: 'Punësim',
				resolve: {
					job: function(resourceService, $stateParams){
						return resourceService.forResource('jobs')
							.get($stateParams.id,'image')
								.then(function(result){
									return result.job;
								});
					},
					categories: function(resourceService){
						return resourceService.forResource('jobCategories')
							.query()
								.then(function(result){
									return result.types;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.jobNew', {
				url: '/job/new',
				templateUrl: '../../frontend_files/views/admin/job.html',
				controller: 'jobAdminController',
				pageTitle: 'Punësim',
				resolve: {
					job: function(resourceService, $stateParams){
						return null;
					},
					categories: function(resourceService){
						return resourceService.forResource('jobCategories')
							.query()
								.then(function(result){
									return result.types;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			});
	});
