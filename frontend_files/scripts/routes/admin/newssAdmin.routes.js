'use strict';
angular.module('marketing.newssAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.news', {
				url: '/news',
				templateUrl: '../../frontend_files/views/admin/newss.html',
				controller: 'newssAdminController',
				pageTitle: 'Lajme',
				resolve: {
					newss: function(resourceService){
						return resourceService.forResource('news')
							.query()
								.then(function(result){
									return result.news;
								});
					}
				}
			})
			.state('marketing.admin.newsEdit', {
				url: '/news/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/news.html',
				controller: 'newsAdminController',
				pageTitle: 'Lajm',
				resolve: {
					news: function(resourceService, $stateParams){
						return resourceService.forResource('news')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.news;
								});
					}
				}
			})
			.state('marketing.admin.newsNew', {
				url: '/news/new',
				templateUrl: '../../frontend_files/views/admin/news.html',
				controller: 'newsAdminController',
				pageTitle: 'news',
				resolve: {
					news: function(resourceService, $stateParams){
						return null;
					}
				}
			});
	});
