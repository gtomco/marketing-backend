'use strict';
angular.module('marketing.admin.routes', [
	'marketing.businessesAdmin.routes',
	'marketing.eventsAdmin.routes',
	'marketing.newssAdmin.routes',
	'marketing.consultingsAdmin.routes',
	'marketing.infoAdmin.routes',
	'marketing.jobsAdmin.routes',
	'marketing.servicesAdmin.routes',
	'marketing.optionsAdmin.routes'
]).config(function ($stateProvider) {
	$stateProvider
		.state('marketing.admin', {
			url: '/admin',
			templateUrl: '../../../frontend_files/views/admin/index.html',
			abstract: true,
			resolve: {
				logged: function($http, $state, ENV){
					return $http.get(ENV.baseEndpoint + 'admin/session')
						.then(function(result){
							return result.data;
						}, function(error){
							$state.go('marketing.login');
						});
				}
			},
			pageTitle: 'Admin'
		})
		.state('marketing.admin.dashboard', {
			url: '',
			templateUrl: '../../../frontend_files/views/admin/dashboard.html',
			pageTitle: 'Admin'
		})
		.state('marketing.login', {
			url: '/login',
			templateUrl: '../../../frontend_files/views/admin/login.html',
			controller: 'loginController',
			pageTitle: 'Login'
		});
});
