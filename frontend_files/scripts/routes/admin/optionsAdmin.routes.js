'use strict';
angular.module('marketing.optionsAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.options', {
				url: '/options',
				templateUrl: '../../frontend_files/views/admin/options.html',
				controller: 'optionsAdminController',
				pageTitle: 'Opsione',
				resolve: {
					slides: function(resourceService){
						return resourceService.forResource('slides')
							.query()
								.then(function(result){
									return result.slides;
								});
					},
					businesses: function(resourceService){
						return resourceService.forResource('businesses')
							.query()
								.then(function(result){
									return result.businesses;
								});
					},
				}
			});
	});
