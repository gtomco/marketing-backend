'use strict';
angular.module('marketing.eventsAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.events', {
				url: '/events',
				templateUrl: '../../frontend_files/views/admin/events.html',
				controller: 'eventsAdminController',
				pageTitle: 'Evente',
				resolve: {
					events: function(resourceService){
						return resourceService.forResource('events')
							.query({include: 'country'})
								.then(function(result){
									return result.events;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.eventEdit', {
				url: '/event/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/event.html',
				controller: 'eventAdminController',
				pageTitle: 'Event',
				resolve: {
					event: function(resourceService, $stateParams){
						return resourceService.forResource('events')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.event;
								});
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.admin.eventNew', {
				url: '/event/new',
				templateUrl: '../../frontend_files/views/admin/event.html',
				controller: 'eventAdminController',
				pageTitle: 'Event',
				resolve: {
					event: function(resourceService, $stateParams){
						return null;
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.query()
								.then(function(result){
									return result.countries;
								});
					}
				}
			});
	});
