'use strict';
angular.module('marketing.consultingsAdmin.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.admin.consulting', {
				url: '/consulting',
				templateUrl: '../../frontend_files/views/admin/consultings.html',
				controller: 'consultingsAdminController',
				pageTitle: 'Shërbime konsulence',
				resolve: {
					consultings: function(resourceService){
						return resourceService.forResource('consulting')
							.query()
								.then(function(result){
									return result.consulting;
								});
					}
				}
			})
			.state('marketing.admin.consultingEdit', {
				url: 'consulting/edit/{id}',
				templateUrl: '../../frontend_files/views/admin/consulting.html',
				controller: 'consultingAdminController',
				pageTitle: 'Shërbim konsulence',
				resolve: {
					consulting: function(resourceService, $stateParams){
						return resourceService.forResource('consulting')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.consulting;
								});
					}
				}
			})
			.state('marketing.admin.consultingNew', {
				url: '/new',
				templateUrl: '../../frontend_files/views/admin/consulting.html',
				controller: 'consultingAdminController',
				pageTitle: 'Shërbim konsulence',
				resolve: {
					consulting: function(resourceService){
						return null;
					}
				}
			});
	});
