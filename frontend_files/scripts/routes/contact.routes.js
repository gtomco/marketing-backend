'use strict';
angular.module('marketing.contact.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.contact', {
				url: '/kontakt',
				templateUrl: '../../frontend_files/views/contact/index.html',
				controller: 'contactController',
				pageTitle: 'Kontakt'
			});
	});
