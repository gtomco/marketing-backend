'use strict';
angular.module('marketing.jobs.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.jobs', {
				url: '/punesime/',
				params: {
					category: null,
					currentType: null
				},
				templateUrl: '../../frontend_files/views/jobs/index.html',
				controller: 'jobsController',
				pageTitle: 'Shërbime punësimi - magnetbiznes.com',
				resolve: {
					currentCategory: function($stateParams){
						return $stateParams.category;
					},
					currentType: function($stateParams){
						return $stateParams.currentType;
					},
					categories: function(resourceService, $stateParams){
						return resourceService.forResource('categories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					types: function(resourceService, $stateParams){
						return resourceService.forResource('jobCategories')
							.query()
								.then(function(result){
									return result.types;
								});
					},
					jobs: function(resourceService, $stateParams){
						return resourceService.forResource('jobs')
							.query({include:'image,category'})
								.then(function(result){
									return result.jobs;
								});
					}
				}
			})
			.state('marketing.job', {
				url: '/punesim/{id}',
				templateUrl: '../../frontend_files/views/jobs/job.html',
				controller: 'jobController',
				pageTitle: 'Shërbime punësimi - magnetbiznes.com',
				resolve: {
					job: function(resourceService, $stateParams){
						return resourceService.forResource('jobs')
							.get($stateParams.id,'image,category')
								.then(function(result){
									return result.job;
								});
					}
				}
			});
	});
