'use strict';
angular.module('marketing.about.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.about', {
				url: '/rreth-nesh',
				templateUrl: '../../frontend_files/views/about/index.html',
				controller: 'aboutController',
				pageTitle: 'Konsulencë marketingu - magnetbiznes.com',
				resolve: {
					about: function(resourceService){
						return resourceService.forResource('about')
							.query({include: 'images'})
								.then(function(result){
									return result.about;
								});
					}
				}
			});
	});
