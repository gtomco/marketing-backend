'use strict';
angular.module('marketing.consulting.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.consulting', {
				url: '/konsulence',
				templateUrl: '../../frontend_files/views/consulting/index.html',
				controller: 'consultingController',
				pageTitle: 'Kush jemi ne - magnetbiznes.com',
				resolve: {
					consultings: function(resourceService){
						return resourceService.forResource('consulting')
							.query({include: 'images'})
								.then(function(result){
									return result.consulting;
								});
					}
				}
			});
	});
