'use strict';
angular.module('marketing.services.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.services', {
				url: '/sherbime/',
				params: {
					category: null
				},
				templateUrl: '../../frontend_files/views/services/index.html',
				controller: 'servicesController',
				pageTitle: 'Shërbime marketingu - magnetbiznes.com',
				resolve: {
					currentCategory: function($stateParams){
						return $stateParams.category;
					},
					categories: function(resourceService, $stateParams){
						return resourceService.forResource('serviceCategories')
							.query()
								.then(function(result){
									return result.categories;
								});
					},
					services: function(resourceService, $stateParams){
						return resourceService.forResource('services')
							.query({include:'images,category'})
								.then(function(result){
									return result.services;
								});
					}
				}
			})
			.state('marketing.service', {
				url: '/sherbim/{id}',
				templateUrl: '../../frontend_files/views/services/service.html',
				controller: 'serviceController',
				pageTitle: 'Shërbime marketingu - magnetbiznes.com',
				resolve: {
					service: function(resourceService, $stateParams){
						return resourceService.forResource('services')
							.get($stateParams.id,'images,category')
								.then(function(result){
									return result.service;
								});
					}
				}
			});
	});
