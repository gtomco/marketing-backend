'use strict';
angular.module('marketing.events.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.events', {
				url: '/evente',
				templateUrl: '../../frontend_files/views/events/index.html',
				controller: 'eventsController',
				pageTitle: 'Evente - magnetbiznes.com',
				resolve: {
					events: function(resourceService){
						return resourceService.forResource('events')
							.query({include: 'country,images'})
								.then(function(result){
									return result.events;
								})
					},
					countries: function(resourceService){
						return resourceService.forResource('countries')
							.get()
								.then(function(result){
									return result.countries;
								});
					}
				}
			})
			.state('marketing.event', {
				url: '/event/{id}',
				templateUrl: '../../frontend_files/views/events/event.html',
				controller: 'eventController',
				pageTitle: 'Evente - magnetbiznes.com',
				resolve: {
					event: function(resourceService, $stateParams){
						return resourceService.forResource('events')
							.get($stateParams.id,'country,images')
								.then(function(result){
									return result.event;
								});
					}
				}
			});
	});
