'use strict';
angular.module('marketing.news.routes', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('marketing.newss', {
				url: '/lajme',
				templateUrl: '../../frontend_files/views/news/index.html',
				controller: 'newssController',
				pageTitle: 'Lajme Biznesi - magnetbiznes.com',
				resolve: {
					news: function(resourceService){
						return resourceService.forResource('news')
							.query({include: 'images'})
								.then(function(result){
									return result.news;
								})
					}
				}
			})
			.state('marketing.news', {
				url: '/lajm/{id}',
				templateUrl: '../../frontend_files/views/news/news.html',
				controller: 'newsController',
				pageTitle: 'Lajme Biznesi - magnetbiznes.com',
				resolve: {
					news: function(resourceService, $stateParams){
						return resourceService.forResource('news')
							.get($stateParams.id,'images')
								.then(function(result){
									return result.news;
								});
					}
				}
			});
	});
