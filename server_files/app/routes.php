<?php

Route::group(array('prefix' => 'api'), function() {
	Route::resource('/countries', 'CountriesController', ['only' => ['index', 'show']]);
	Route::resource('/categories', 'CategoriesController', ['only' => ['index']]);
	Route::resource('/serviceCategories', 'ServiceCategoriesController', ['only' => ['index']]);
	Route::resource('/services', 'ServicesController', ['only' => ['index', 'show']]);
	Route::resource('/businesses', 'BusinessesController', ['only' => ['index', 'show']]);
	Route::resource('/events', 'EventsController', ['only' => ['index', 'show']]);
	Route::resource('/news', 'NewsController', ['only' => ['index', 'show']]);
	Route::resource('/consulting', 'ConsultingController', ['only' => ['index','show']]);
	Route::resource('/about', 'AboutController', ['only' => ['index','show']]);
	Route::resource('/jobCategories', 'JobCategoriesController', ['only' => ['index']]);
	Route::resource('/jobs', 'JobsController', ['only' => ['index','show']]);
	Route::resource('/images', 'ImagesController', ['only' => ['index','show']]);
	Route::resource('/slides', 'SlidesController', ['only' => ['index','show']]);
	Route::resource('/email', 'EmailController', ['only' => ['store']]);
	Route::group(array('before' => 'auth'), function() {
		Route::resource('/countries', 'CountriesController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/categories', 'CategoriesController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/serviceCategories', 'ServiceCategoriesController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/services', 'ServicesController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/businesses', 'BusinessesController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/events', 'EventsController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/news', 'NewsController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/consulting', 'ConsultingController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/about', 'AboutController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/jobs', 'JobsController', ['only' => ['destroy', 'store', 'update']]);
		Route::resource('/images', 'ImagesController', ['only' => ['destroy']]);
		Route::resource('/slides', 'SlidesController', ['only' => ['destroy', 'store', 'update']]);
		Route::post('/images/{business}/{id}', array('uses' => 'ImagesController@store'));
		Route::post('/pdf/{id}', array('uses' => 'ImagesController@storePdf'));
		Route::delete('/pdf/{id}', array('uses' => 'ImagesController@destroypdf'));
	});
});

Route::group(array('prefix' => 'admin'), function() {
	Route::get('/session', array('uses' => 'AuthenticateController@session'));
	Route::group(array('before' => 'auth'), function() {
		Route::get('/logout', array('uses' => 'AuthenticateController@logout'));
	});
	Route::post('/login', array('uses' => 'AuthenticateController@login'));
});
