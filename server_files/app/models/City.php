<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class City extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cities';

	public function businesses()
	{
		return $this->hasMany('Business');
	}

	public function country()
	{
		return $this->belongsTo('Country');
	}

}

?>
