<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Country extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'countries';

	public function businesses()
	{
		return $this->hasMany('Business');
	}

	public function jobs()
	{
		return $this->hasMany('Job');
	}

	public function cities()
	{
		return $this->hasMany('City');
	}

}

?>