<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Job extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'jobs';

	public function image()
	{
		return $this->hasOne('Image');
	}

	public function country()
	{
		return $this->belongsTo('Country');
	}

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function type()
	{
		return $this->belongsTo('JobCategory');
	}

}

?>
