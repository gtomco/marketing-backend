<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class EventTable extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'events';

	public function images()
	{
		return $this->hasMany('Image')->orderBy('priority');
	}

	public function country()
	{
		return $this->belongsTo('Country');
	}

}

?>