<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Consulting extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'consultings';

	public function images()
	{
		return $this->hasMany('Image')->orderBy('priority');
	}

}

?>