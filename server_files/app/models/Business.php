<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Business extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'businesses';

	public function images()
	{
		return $this->hasMany('Image')->orderBy('priority');
	}

	public function country()
	{
		return $this->belongsTo('Country');
	}

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function city()
	{
		return $this->belongsTo('City');
	}

}

?>