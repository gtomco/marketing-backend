<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Image extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'images';

	public function event()
	{
		return $this->belongsTo('EventTable');
	}

	public function news()
	{
		return $this->belongsTo('News');
	}

	public function job()
	{
		return $this->belongsTo('Job');
	}

	public function service()
	{
		return $this->belongsTo('Service');
	}

	public function consulting()
	{
		return $this->belongsTo('Consulting');
	}

	public function business()
	{
		return $this->belongsTo('Business');
	}

}

?>