<?php

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Service extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'services';

	public function images()
	{
		return $this->hasMany('Image')->orderBy('priority');
	}

	public function category()
	{
		return $this->belongsTo('ServiceCategory');
	}

}

?>