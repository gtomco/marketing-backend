<?php

class SlidesController extends BaseController {

	public function index()
	{
		$slides = new stdClass();
		$slides->slides = Slide::with(['business', 'business.images'])->orderBy('priority', 'ASC')->get();
		return Response::json($slides);
	}

	public function show($id)
	{
		$slides = new stdClass();
		$slides->slides = Slide::with('business')->where('id', '=', $id)->first();
		return Response::json($slides);
	}

	public function destroy($id)
	{
		Slide::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$slide = new Slide;
		$slide->priority = Input::get('priority');
		$slide->business_id = Input::get('business_id');
		$slide->save();
		$id = $slide->id;
		$slide = Slide::with('business')->where('id', '=', $id)->first();
		return Response::json($slide);
	}

	public function update($id)
	{
		$slide = Slide::find($id);
		$slide->priority = Input::get('priority');
		$slide->business_id = Input::get('business_id');
		$slide->save();
		$slide = Slide::with('business')->where('id', '=', $id)->first();
		return Response::json($slide);
	}

}
