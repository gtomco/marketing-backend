<?php

class EventsController extends BaseController {

	public function index()
	{
		$events = new stdClass();
		$events->events = EventTable::with($this->include)->where('date', '>=', \Carbon\Carbon::now())->orderBy('date', 'DESC')->get();
		return Response::json($events);
	}

	public function show($id)
	{
		$event = new stdClass();
		$event->event = EventTable::with($this->include)->where('id', '=', $id)->first();
		return Response::json($event);
	}

	public function destroy($id)
	{
		EventTable::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$event = new EventTable;
		$event->name = Input::get('name');
		$event->description = Input::get('description');
		$event->address = Input::get('address');
		$event->phone = Input::get('phone');
		$event->email = Input::get('email');
		$event->coords_x = Input::get('coords_x');
		$event->coords_y = Input::get('coords_y');
		$event->country_id = Input::get('country_id');
		//$event->date = Input::get('date');
		$event->save();
		return Response::json($event);
	}

	public function update($id)
	{
		$event = EventTable::find($id);
		$event->name = Input::get('name');
		$event->description = Input::get('description');
		$event->address = Input::get('address');
		$event->phone = Input::get('phone');
		$event->email = Input::get('email');
		$event->coords_x = Input::get('coords_x');
		$event->coords_y = Input::get('coords_y');
		$event->country_id = Input::get('country_id');
		//$event->date = Input::get('date');
		$event->save();
		return Response::json($event);
	}

}
