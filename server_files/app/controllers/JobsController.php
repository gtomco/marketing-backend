<?php

class JobsController extends BaseController {

	public function index()
	{
		$jobs = new stdClass();
		$jobs->jobs = Job::with($this->include)->get();
		return Response::json($jobs);
	}

	public function show($id)
	{
		$job = new stdClass();
		$job->job = Job::with($this->include)->where('id', '=', $id)->first();
		return Response::json($job);
	}

	public function destroy($id)
	{
		Job::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$job = new Job;
		$job->name = Input::get('name');
		$job->description = Input::get('description');
		$job->phone = Input::get('phone');
		$job->email = Input::get('email');
		$job->country_id = Input::get('country_id');
		$job->type_id = Input::get('type_id');
		$job->save();
		return Response::json($job);
	}

	public function update($id)
	{
		$job = Job::find($id);
		$job->name = Input::get('name');
		$job->description = Input::get('description');
		$job->phone = Input::get('phone');
		$job->email = Input::get('email');
		$job->country_id = Input::get('country_id');
		$job->type_id = Input::get('type_id');
		$job->save();
		return Response::json($job);
	}

}
