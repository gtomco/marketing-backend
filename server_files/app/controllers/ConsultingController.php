<?php

class ConsultingController extends BaseController {

	public function index()
	{
		$consulting = new stdClass();
		$consulting->consulting = Consulting::with($this->include)->get();
		return Response::json($consulting);
	}

	public function show($id)
	{
		$consulting = new stdClass();
		$consulting->consulting = Consulting::with($this->include)->where('id', '=', $id)->first();
		return Response::json($consulting);
	}

	public function destroy($id)
	{
		Consulting::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$consulting = new Consulting;
		$consulting->heading = Input::get('heading');
		$consulting->content = Input::get('content');
		$consulting->save();
		return Response::json($consulting);
	}

	public function update($id)
	{
		$consulting = Consulting::find($id);
		$consulting->heading = Input::get('heading');
		$consulting->content = Input::get('content');
		$consulting->save();
		return Response::json($consulting);
	}

}
