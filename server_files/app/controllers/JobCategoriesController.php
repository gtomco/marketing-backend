<?php

class JobCategoriesController extends BaseController {

	public function index()
	{
		$types = new stdClass();
		$types->types = JobCategory::with($this->include)->get();
		return Response::json($types);
	}

}
