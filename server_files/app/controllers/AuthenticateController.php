<?php

	class AuthenticateController extends BaseController{

		public function login(){
			$userData = array(
				'email'=> Input::get('email'),
				'password'=> Input::get('password')
			);
			try {
				Auth::attempt($userData, true);
			} catch (Exception $e) {
				return App::abort(401, 'Authorization failed.');
			}
		}

		public function logout(){
			try {
				return Auth::logout();
			} catch (Exception $e) {
				return App::abort(401, 'Authorization failed.');
			}
		}

		public function session(){
			try {
				if (Auth::check()) {
					return Auth::user()->remember_token;
				}
				else {
					return App::abort(401, 'Authorization failed.');
				}
			} catch (Exception $e) {
				return App::abort(401, 'Authorization failed.');
			}
		}
	}

?>
