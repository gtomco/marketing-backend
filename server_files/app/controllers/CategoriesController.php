<?php

class CategoriesController extends BaseController {

	public function index()
	{
		$categories = new stdClass();
		$categories->categories = Category::with($this->include)->get();
		return Response::json($categories);
	}

}
