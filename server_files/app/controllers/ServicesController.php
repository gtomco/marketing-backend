<?php

class ServicesController extends BaseController {

	public function index()
	{
		$services = new stdClass();
		$services->services = Service::with($this->include)->get();
		return Response::json($services);
	}

	public function show($id)
	{
		$service = new stdClass();
		$service->service = Service::with($this->include)->where('id', '=', $id)->first();
		return Response::json($service);
	}

	public function destroy($id)
	{
		Service::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$service = new Service;
		$service->name = Input::get('name');
		$service->content = Input::get('content');
		$service->address = Input::get('address');
		$service->phone = Input::get('phone');
		$service->email = Input::get('email');
		$service->category_id = Input::get('category_id');
		$service->save();
		return Response::json($service);
	}

	public function update($id)
	{
		$service = Service::find($id);
		$service->name = Input::get('name');
		$service->content = Input::get('content');
		$service->address = Input::get('address');
		$service->phone = Input::get('phone');
		$service->email = Input::get('email');
		$service->category_id = Input::get('category_id');
		$service->save();
		return Response::json($service);
	}

}
