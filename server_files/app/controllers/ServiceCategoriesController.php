<?php

class ServiceCategoriesController extends BaseController {

	public function index()
	{
		$categories = new stdClass();
		$categories->categories = ServiceCategory::with($this->include)->get();
		return Response::json($categories);
	}

}
