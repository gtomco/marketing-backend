<?php

class NewsController extends BaseController {

	public function index()
	{
		$news = new stdClass();
		$news->news = News::with($this->include)->orderBy('created_at', 'DESC')->get();
		return Response::json($news);
	}

	public function show($id)
	{
		$news = new stdClass();
		$news->news = News::with($this->include)->where('id', '=', $id)->first();
		return Response::json($news);
	}

	public function destroy($id)
	{
		News::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$news = new News;
		$news->title = Input::get('title');
		$news->description = Input::get('description');
		$news->save();
		return Response::json($news);
	}

	public function update($id)
	{
		$news = News::find($id);
		$news->title = Input::get('title');
		$news->description = Input::get('description');
		$news->save();
		return Response::json($news);
	}

}
