<?php

class BusinessesController extends BaseController {

	public function index()
	{
		$businesses = new stdClass();
		$businesses->businesses = Business::with($this->include)->get();
		return Response::json($businesses);
	}

	public function show($id)
	{
		$business = new stdClass();
		$business->business = Business::with($this->include)->where('id', '=', $id)->first();
		return Response::json($business);
	}

	public function destroy($id)
	{
		Business::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$business = new Business;
		$business->name = Input::get('name');
		$business->description = Input::get('description');
		$business->address = Input::get('address');
		$business->phone = Input::get('phone');
		$business->email = Input::get('email');
		$business->coords_x = Input::get('coords_x');
		$business->coords_y = Input::get('coords_y');
		$business->category_id = Input::get('category_id');
		$business->city_id = Input::get('city_id');
		$business->country_id = Input::get('country_id');
		$business->save();
		return Response::json($business);
	}

	public function update($id)
	{
		$business = Business::find($id);
		$business->name = Input::get('name');
		$business->description = Input::get('description');
		$business->address = Input::get('address');
		$business->phone = Input::get('phone');
		$business->email = Input::get('email');
		$business->coords_x = Input::get('coords_x');
		$business->coords_y = Input::get('coords_y');
		$business->category_id = Input::get('category_id');
		$business->city_id = Input::get('city_id');
		$business->country_id = Input::get('country_id');
		$business->save();
		return Response::json($business);
	}

}
