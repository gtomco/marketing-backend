<?php

class AboutController extends BaseController {

	public function index()
	{
		$about = new stdClass();
		$about->about = About::with($this->include)->get();
		return Response::json($about);
	}

	public function show($id)
	{
		$about = new stdClass();
		$about->about = About::with($this->include)->where('id', '=', $id)->first();
		return Response::json($about);
	}

	public function destroy($id)
	{
		About::destroy($id);
		return Response::json(200);
	}

	public function store()
	{
		$about = new About;
		$about->header = Input::get('header');
		$about->content = Input::get('content');
		$about->save();
		return Response::json($about);
	}

	public function update($id)
	{
		$about = About::find($id);
		$about->header = Input::get('header');
		$about->content = Input::get('content');
		$about->save();
		return Response::json($about);
	}

}
