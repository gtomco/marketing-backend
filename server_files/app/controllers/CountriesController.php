<?php

class CountriesController extends BaseController {

	public function index()
	{
		$countries = new stdClass();
		$countries->countries = Country::with($this->include)->get();
		return Response::json($countries);
	}

	public function show($slug)
	{
		$country = new stdClass();
		$country->country = Country::with($this->include)->where('slug', '=', $slug)->first();
		return Response::json($country);
	}

}
