<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected $fields;

	public function __construct(){

		if (Input::has('include')) {
			$this->include = explode(',', Input::get('include'));
		}
		else {
			$this->include = [];
		}
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
