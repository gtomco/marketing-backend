<?php

class ImagesController extends BaseController {

	public function index()
	{
		$images = new stdClass();
		$images->images = Image::with($this->include)->get();
		return Response::json($images);
	}

	public function show($id)
	{
		$image = new stdClass();
		$image->image = Image::with($this->include)->where('id', '=', $id)->first();
		return Response::json($image);
	}

	public function destroy($id)
	{
		$image = Image::find($id);
		$filename = public_path() . '//uploads//' . $image->path . '.' . $image->ext;
		if (File::exists($filename)) {
			File::delete($filename);
		}
		$image->delete();
		return Response::json(200);
	}

	public function destroypdf($id)
	{
		$job = Job::find($id);
		$filename = public_path() . '//uploads//' . $job->path . '.' . $job->ext;
		$job->path = null;
		$job->ext = null;
		$job->save();
		if (File::exists($filename)) {
			File::delete($filename);
		}
		return Response::json(200);
	}

	public function store($resource, $id)
	{
		if(Input::hasFile('file'))
		{
			$file = Input::file('file');
			$fileName = str_random(25);
			$fileExtension = $file->getClientOriginalExtension();
			$file->move(public_path() . '/uploads', $fileName . '.' . $fileExtension);
			$image = ImageIntervention::make(public_path() . '/uploads//' . $fileName . '.' . $fileExtension);
			$image->fit(1312, 552)->save(null, 85);
			//$image->fit(200)->save(public_path() . '/uploads//thumbs//' . $fileName . '.' . $fileExtension, 55);
			$image = new Image;
			$image->path = $fileName;
			$image->ext = $fileExtension;
			$image->caption = null;
			switch ($resource) {
				case 'business': $resource = 'business_id'; break;
				case 'news': $resource = 'news_id'; break;
				case 'event': $resource = 'event_table_id'; break;
				case 'service': $resource = 'service_id'; break;
				case 'consulting': $resource = 'consulting_id'; break;
				case 'job': $resource = 'job_id'; break;
				case 'about': $resource = 'about_id'; break;
				default: return Response::json(404); break;
			}
			$image[$resource] = $id;
			$image->save();
			return $image;
		}
	}

	public function storepdf($id)
	{
		if(Input::hasFile('file'))
		{
			$file = Input::file('file');
			$fileName = str_random(25);
			$fileExtension = $file->getClientOriginalExtension();
			$file->move(public_path() . '/uploads', $fileName . '.' . $fileExtension);
			$job = Job::find($id);
			$job->path = $fileName;
			$job->ext = $fileExtension;
			$job->save();
			return $job;
		}
	}

}
