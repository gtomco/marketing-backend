<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function($image){
			$image -> increments('id');
			$image -> string('path');
			$image -> string('ext');
			$image -> integer('priority')->unsigned();
			$image -> text('caption')->nullable();
			$image -> integer('business_id')->unsigned()->nullable();
			$image -> foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
			$image -> integer('news_id')->unsigned()->nullable();
			$image -> foreign('news_id')->references('id')->on('newss')->onDelete('cascade');
			$image -> integer('event_table_id')->unsigned()->nullable();
			$image -> foreign('event_table_id')->references('id')->on('events')->onDelete('cascade');
			$image -> integer('service_id')->unsigned()->nullable();
			$image -> foreign('service_id')->references('id')->on('services')->onDelete('cascade');
			$image -> integer('consulting_id')->unsigned()->nullable();
			$image -> foreign('consulting_id')->references('id')->on('consultings')->onDelete('cascade');
			$image -> integer('job_id')->unsigned()->nullable();
			$image -> foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
			$image -> integer('about_id')->unsigned()->nullable();
			$image -> foreign('about_id')->references('id')->on('about')->onDelete('cascade');
			$image -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}

}
