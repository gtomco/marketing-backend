<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Slides extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slides', function($slide){
			$slide -> increments('id');
			$slide -> integer('priority');
			$slide -> integer('business_id')->unsigned()->nullable();
			$slide -> foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
			$slide -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slides');
	}

}
