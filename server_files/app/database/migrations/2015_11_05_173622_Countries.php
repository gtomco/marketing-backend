<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Countries extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function($country){
			$country -> increments('id');
			$country -> string('name');
			$country -> string('slug');
			$country -> string('email');
			$country -> string('phone');
			$country -> string('icon')->nullable();
			$country -> string('color')->nullable();
			$country -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}