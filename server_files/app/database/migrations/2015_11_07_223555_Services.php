<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Services extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('services', function($service){
			$service -> increments('id');
			$service -> string('name');
			$service -> string('address')->nullable();
			$service -> string('phone')->nullable();
			$service -> string('email')->nullable();
			$service -> text('content')->nullable();
			$service -> integer('category_id')->unsigned()->nullable();
			$service -> foreign('category_id')->references('id')->on('serviceCategories');
			$service -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('services');
	}

}
