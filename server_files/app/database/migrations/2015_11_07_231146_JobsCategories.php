<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobsCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobCategories', function($jobCategory){
			$jobCategory -> increments('id');
			$jobCategory -> string('name');
			$jobCategory -> string('icon')->nullable();
			$jobCategory -> string('color')->nullable();
			$jobCategory -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobCategories');
	}

}
