<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function($city){
			$city -> increments('id');
			$city -> string('name');
			$city -> string('slug');
			$city -> integer('country_id')->unsigned()->nullable();
			$city -> foreign('country_id')->references('id')->on('countries');
			$city -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}