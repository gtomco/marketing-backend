<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jobs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function($job){
			$job -> increments('id');
			$job -> string('name');
			$job -> string('email')->nullable();
			$job -> string('phone')->nullable();
			$job -> text('description')->nullable();
			$job -> integer('category_id')->unsigned()->nullable();
			$job -> foreign('category_id')->references('id')->on('categories');
			$job -> integer('country_id')->unsigned()->nullable();
			$job -> foreign('country_id')->references('id')->on('countries');
			$job -> integer('type_id')->unsigned()->nullable();
			$job -> foreign('type_id')->references('id')->on('jobCategories');
			$job -> string('path')->nullable();
			$job -> string('ext')->nullable();
			$job -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
