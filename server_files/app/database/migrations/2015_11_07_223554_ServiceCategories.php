<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('serviceCategories', function($serviceCategory){
			$serviceCategory -> increments('id');
			$serviceCategory -> string('name');
			$serviceCategory -> string('icon')->nullable();
			$serviceCategory -> string('color')->nullable();
			$serviceCategory -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('serviceCategories');
	}

}
