<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Businesses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function($business){
			$business -> increments('id');
			$business -> string('name');
			$business -> string('description')->nullable();
			$business -> string('address')->nullable();
			$business -> string('phone')->nullable();
			$business -> string('email')->nullable();
			$business -> string('coords_x')->nullable();
			$business -> string('coords_y')->nullable();
			$business -> integer('category_id')->unsigned()->nullable();
			$business -> foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			$business -> integer('city_id')->unsigned()->nullable();
			$business -> foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
			$business -> integer('country_id')->unsigned()->nullable();
			$business -> foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
			$business -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('businesses');
	}

}
