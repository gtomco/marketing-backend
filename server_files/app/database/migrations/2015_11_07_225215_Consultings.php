<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Consultings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consultings', function($serviceCategory){
			$serviceCategory -> increments('id');
			$serviceCategory -> string('header');
			$serviceCategory -> text('content')->nullable();
			$serviceCategory -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consultings');
	}

}
