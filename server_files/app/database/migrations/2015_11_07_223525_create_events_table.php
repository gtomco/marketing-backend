<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function($event){
			$event -> increments('id');
			$event -> string('name');
			$event -> text('description')->nullable();
			$event -> string('address')->nullable();
			$event -> string('phone')->nullable();
			$event -> string('email')->nullable();
			$event -> integer('country_id')->unsigned()->nullable();
			$event -> foreign('country_id')->references('id')->on('countries');
			$event -> string('coords_x')->nullable();
			$event -> string('coords_y')->nullable();
			$event -> dateTime('date');
			$event -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
