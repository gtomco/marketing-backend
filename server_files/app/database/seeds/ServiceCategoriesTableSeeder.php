<?php
	class ServiceCategoriesTableSeeder extends Seeder {
		public function run(){
			ServiceCategory::truncate();
			$category = ServiceCategory::create(array(
				'name' => 'Konsulent Financiar',
				'icon' => 'flaticon-marketing10',
				'color' => '#9D8F62'
			));
			$category = ServiceCategory::create(array(
				'name' => 'Ekonomist & Kontabilist',
				'icon' => 'flaticon-dollar9',
				'color' => '#9D8F62'
			));
			$category = ServiceCategory::create(array(
				'name' => 'Zyra Ligjore',
				'icon' => 'flaticon-judge1',
				'color' => '#9D8F62'
			));
			$category = ServiceCategory::create(array(
				'name' => 'Avokati & Jurist',
				'icon' => 'flaticon-woman145',
				'color' => '#9D8F62'
			));

		}
	}
?>
