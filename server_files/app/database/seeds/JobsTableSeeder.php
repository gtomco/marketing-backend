<?php
	class JobsTableSeeder extends Seeder {
		public function run(){
			Job::truncate();
			$faker = Faker\Factory::create();
			for ($i=1; $i <= 25; $i++) {
				$business = Job::create(array(
					'name' => ($i%2)?'Punëdhënës ' . $i:'Punëmarrës ' . $i,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'description' => $faker->text,
					'category_id' => ($i%5)+1,
					'country_id' => ($i%4)+1,
					'type_id' => ($i%2)+1,
					'path' => 'test',
					'ext' => 'pdf'
				));
			}
		}
	}
?>
