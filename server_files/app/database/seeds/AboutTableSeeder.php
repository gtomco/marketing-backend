<?php
	class AboutTableSeeder extends Seeder {
		public function run(){
			About::truncate();
			$faker = Faker\Factory::create();
			for ($i=1; $i <= 5; $i++) {
				$about = About::create(array(
					'header' => $faker->words($nb = rand(1,3), $asText = true),
					'content' => $faker->sentences($nb = rand(8,15), $asText = true)
				));
			}
		}
	}
?>