<?php
	class NewsTableSeeder extends Seeder {

		public function run(){
			$faker = Faker\Factory::create();
			News::truncate();
			for ($i=1; $i <= 25; $i++) {
				$news = News::create(array(
					'title' => 'News ' . $i,
					'description' => $faker->text
				));
			}
		}
	}
?>