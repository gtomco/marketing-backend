<?php
	class ServicesTableSeeder extends Seeder {
		public function run(){
			Service::truncate();
			$faker = Faker\Factory::create();
			for ($i=1; $i <= 32 ; $i++) {
				$service = Service::create(array(
					'name' => $faker->name,
					'content' => $faker->text,
					'category_id' => ($i%4)+1
				));
			}
		}
	}
?>
