<?php
	class UsersTableSeeder extends Seeder {
		public function run(){
			User::truncate();
			$user = User::create(array(
				'name' => 'Admin admin',
				'email' => 'admin@magnetbiznes.com',
				'password' => '$2a$04$QGTyZOfX4sp8MavEn5G4d.ablHjhXBShO7R3CC89ropPScGDv6iQm',
				'role' => 1
			));
			$user = User::create(array(
				'name' => 'Info',
				'email' => 'info@magnetbiznes.com',
				'password' => '$2a$04$QGTyZOfX4sp8MavEn5G4d.ablHjhXBShO7R3CC89ropPScGDv6iQm',
				'role' => 1
			));
		}
	}
?>
