<?php
	class ConsultingTableSeeder extends Seeder {
		public function run(){
			Consulting::truncate();
			$faker = Faker\Factory::create();
			for ($i=1; $i <= 5; $i++) { 
				$consulting = Consulting::create(array(
					'header' => $faker->words($nb = rand(1,3), $asText = true),
					'content' => $faker->sentences($nb = rand(8,15), $asText = true)
				));
			}
		}
	}
?>