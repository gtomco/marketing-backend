<?php
	class BusinessesTableSeeder extends Seeder {

		public function run(){
			$faker = Faker\Factory::create();
			Business::truncate();
			for ($i=1; $i <= 88; $i++) {
				$business = Business::create(array(
					'name' => 'Biznes ' . $i,
					'description' => $faker->text,
					'address' => $faker->streetAddress,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'coords_x' => $faker->latitude,
					'coords_y' => $faker->longitude,
					'category_id' => ($i%22)+1,
					'city_id' => 1,
					'country_id' => 1
				));
				$business = Business::create(array(
					'name' => 'Biznes ' . $i,
					'description' => $faker->text,
					'address' => $faker->streetAddress,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'coords_x' => $faker->latitude,
					'coords_y' => $faker->longitude,
					'category_id' => ($i%22)+1,
					'city_id' => 2,
					'country_id' => 2
				));
				$business = Business::create(array(
					'name' => 'Biznes ' . $i,
					'description' => $faker->text,
					'address' => $faker->streetAddress,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'coords_x' => $faker->latitude,
					'coords_y' => $faker->longitude,
					'category_id' => ($i%22)+1,
					'city_id' => 3,
					'country_id' => 3
				));
				$business = Business::create(array(
					'name' => 'Biznes ' . $i,
					'description' => $faker->text,
					'address' => $faker->streetAddress,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'coords_x' => $faker->latitude,
					'coords_y' => $faker->longitude,
					'category_id' => ($i%22)+1,
					'city_id' => 4,
					'country_id' => 4
				));
			}
		}
	}
?>
