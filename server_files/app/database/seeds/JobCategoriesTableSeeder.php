<?php
	class JobCategoriesTableSeeder extends Seeder {
		public function run(){
			JobCategory::truncate();
			$category = JobCategory::create(array(
				'name' => 'Punëmarrës',
				'color' => '#9D8F62',
				'icon' => 'flaticon-employee7'
			));
			$category = JobCategory::create(array(
				'name' => 'Punëdhënës',
				'color' => '#9D8F62',
				'icon' => 'flaticon-employee1'
			));
		}
	}
?>
