<?php
	class CountriesTableSeeder extends Seeder {
		public function run(){
			Country::truncate();
			City::truncate();
			$country = Country::create(array(
				'name' => 'Shqipëri',
				'slug' => 'shqiperi',
				'email' => 'info@magnetbiznes.com',
				'phone' => '+355696969696',
				'icon' => 'map-al',
				'color' => '#B60000'
			));
			$city = City::create(array(
				'name' => 'Tiranë',
				'slug' => 'tirane',
				'country_id' => 1
			));
			$country = Country::create(array(
				'name' => 'Kosovë',
				'slug' => 'kosove',
				'email' => 'info@magnetbiznes.com',
				'phone' => '+355696969696',
				'icon' => 'map-xk',
				'color' => '#3F008A'
			));
			$city = City::create(array(
				'name' => 'Prishtinë',
				'slug' => 'prishtine',
				'country_id' => 2
			));
			$country = Country::create(array(
				'name' => 'Maqedoni',
				'slug' => 'maqedoni',
				'email' => 'info@magnetbiznes.com',
				'phone' => '+355696969696',
				'icon' => 'map-mk',
				'color' => '#F7C600'
			));
			$city = City::create(array(
				'name' => 'Shkup',
				'slug' => 'shkup',
				'country_id' => 3
			));
			$country = Country::create(array(
				'name' => 'Mali i zi',
				'slug' => 'malizi',
				'email' => 'info@magnetbiznes.com',
				'phone' => '+355696969696',
				'icon' => 'map-me',
				'color' => '#009900'
			));
			$city = City::create(array(
				'name' => 'Podgoricë',
				'slug' => 'podgorice',
				'country_id' => 4
			));
		}
	}
?>