<?php
	class ImagesTableSeeder extends Seeder {
		public function run(){
			Image::truncate();
			$faker = Faker\Factory::create();
			for ($i=1; $i <= 3; $i++) {
				$image = Image::create(array(
					'path' => $i,
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'business_id' => $i
				));
			}
			for ($i=4; $i <= 352; $i++) {
				$image = Image::create(array(
					'path' => 'business' . rand(1,3),
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'business_id' => $i
				));
			}
			for ($i=1; $i <= 25; $i++) {
				$image = Image::create(array(
					'path' => 'event' . rand(1,3),
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'event_table_id' => $i
				));
			}
			for ($i=1; $i <= 25; $i++) {
				$image = Image::create(array(
					'path' => 'news',
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'news_id' => $i
				));
			}
			for ($i=1; $i <= 32; $i++) {
				$image = Image::create(array(
					'path' => (($i%3)+1)==1?'user':(($i%3)+1)==2?'law':'bank',
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'service_id' => ($i%25)+1
				));
			}
			for ($i=1; $i <= 5; $i++) {
				$image = Image::create(array(
					'path' => 'paper',
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'consulting_id' => $i
				));
			}
			for ($i=1; $i <= 5; $i++) {
				$image = Image::create(array(
					'path' => 'paper',
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => $faker->text,
					'about_id' => $i
				));
			}
			for ($i=1; $i <= 25; $i++) {
				$image = Image::create(array(
					'path' => 'user',
					'ext' => 'jpg',
					'priority' => 0,
					'caption' => null,
					'job_id' => $i
				));
			}
		}
	}
?>
