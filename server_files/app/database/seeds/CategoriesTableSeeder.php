<?php
	class CategoriesTableSeeder extends Seeder {
		public function run(){
			Category::truncate();
			$category = Category::create(array(
				'name' => 'Bare',
				'icon' => 'flaticon-tea24',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Restorante',
				'icon' => 'flaticon-cutlery6',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Ushqime',
				'icon' => 'flaticon-healthy-food4',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Pije',
				'icon' => 'flaticon-alcohol93',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Clube',
				'icon' => 'flaticon-man204',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Butik',
				'icon' => 'flaticon-tshirt18',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Sallone Bukurie',
				'icon' => 'flaticon-woman71',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Fitnes',
				'icon' => 'flaticon-weightlifter3',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Elektronikë & Service',
				'icon' => 'flaticon-vacuum6',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Organizata & OJQ',
				'icon' => 'flaticon-charity5',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Imobiliare',
				'icon' => 'flaticon-house34',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Agjenci Udhëtimi',
				'icon' => 'flaticon-black401',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Kompani Ndërtimi',
				'icon' => 'flaticon-construction15',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Oil & Gaz',
				'icon' => 'flaticon-gas-station',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Transport',
				'icon' => 'flaticon-vehicle12',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Telekomunikacion',
				'icon' => 'flaticon-phone391',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Klinikë Dentare',
				'icon' => 'flaticon-tooth',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Dyqane Këpucësh',
				'icon' => 'flaticon-clothing363',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Kozmetikë',
				'icon' => 'flaticon-beauty11',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Salla',
				'icon' => 'flaticon-audience1',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Qendra Trajnimi',
				'icon' => 'flaticon-seo40',
				'color' => '#9D8F62'
			));
			$category = Category::create(array(
				'name' => 'Call Center',
				'icon' => 'flaticon-call37',
				'color' => '#9D8F62'
			));
		}
	}
?>
