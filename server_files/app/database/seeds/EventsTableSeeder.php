<?php
	class EventsTableSeeder extends Seeder {

		public function run(){
			$faker = Faker\Factory::create();
			EventTable::truncate();
			for ($i=1; $i <= 25; $i++) {
				$event = EventTable::create(array(
					'name' => 'Event ' . $i,
					'description' => $faker->text,
					'address' => $faker->streetAddress,
					'phone' => $faker->phoneNumber,
					'email' => $faker->email,
					'country_id' => ($i%4)+1,
					'coords_x' => $faker->latitude,
					'coords_y' => $faker->longitude,
					'date' => $faker->dateTimeBetween($startDate = '2 days', $endDate = '1 week')
				));
			}
		}
	}
?>
