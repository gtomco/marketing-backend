<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		$this->call('UsersTableSeeder');
		$this->call('CountriesTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('BusinessesTableSeeder');
		$this->call('EventsTableSeeder');
		$this->call('ImagesTableSeeder');
		$this->call('NewsTableSeeder');
		$this->call('ServiceCategoriesTableSeeder');
		$this->call('ServicesTableSeeder');
		$this->call('ConsultingTableSeeder');
		$this->call('AboutTableSeeder');
		$this->call('JobCategoriesTableSeeder');
		$this->call('JobsTableSeeder');
		$this->call('SlidesTableSeeder');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}
