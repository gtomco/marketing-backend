<title>
	Page not found
</title>
<link href="{{ asset('public_files/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('public_files/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('public_files/css/sb-admin.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('public_files/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('public_files/js/bootstrap.min.js') }}"></script>
<style>
	.big-title{
		font-size: 8.0em;
	}
	.footer{
		position:absolute;
		bottom:0px;
		background-color: #d7d7d7;
		width:100%;
		padding: 10px 20px;
		text-align: center;
	}
</style>
<div class="container">
	<div class="row text-center">
		<div class="col-sm-offset-4 col-sm-4">
			<h1 class="big-title">
				<span class="fa fa-chain-broken"></span>
			</h1>
			<h1><span>404 - Page not found</span></h1>
			<h4 class="text-muted">You may have entered a wrong url.</h4>
			<h4 class="text-muted">Try going back to our <a href="/">homepage</a></h4>
		</div>
	</div>
</div>
<div class="footer">
	<div class="row">
		<div class="col-sm-12">
			<p>Marketing.</p>
		</div>
		<div class="col-sm-12">
			<p>Copyright <a href="http://better.network">better.</a> &copy {{date('Y')}} </p>
		</div>
	</div>
</div>